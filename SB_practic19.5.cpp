﻿#include <iostream>
#include <string>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "Animal voice" << "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!" << "\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Meow!" << "\n";
    }
};

class Сow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Moo!" << "\n";
    }
};

int main()
{
    const int numAnimals = 3;
    Animal* animalArray[numAnimals];

    Dog dog;
    Cat cat;
    Сow cow;

    animalArray[0] = &dog;
    animalArray[1] = &cat;
    animalArray[2] = &cow;

    for (int i = 0; i < numAnimals; ++i)
    {
        animalArray[i]->Voice();
    }

    return 0;
}
